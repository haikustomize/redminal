# Redminal
---

Profile file to alter the aspect of Haiku's terminal PS1 prompt adding some color.
This is a simple modification on the default one. Nothing fancy.

In order to use it, should be copied to the /boot/home/config/settings folder

![alt text][logo]

[logo]:redminal.png
