echo -e "$(date)\nGood day \e[1m$USER \e[0m \n"

# this is the unicode triangle
# in order to see this triangle, we need a font with extended symbols
# like any Nerd Font
CHAR="'\uE0B0'"

# background for the left side is RGB 115,115,115
# for foregrounds: 38;2
# for backgrounds: 48;2
LBGCOLOR="\[\033[48;2;115;115;115m\]"
LFGCOLOR="\[\033[38;2;255;255;255m\]"
LBGDK="\[\033[38;2;115;115;115m\]"

LEFT="\[\e$LBGCOLOR$LFGCOLOR$USER@$HOSTNAME"

# bg color set to orange
# fg color set to slate 
# RGB colors as set from the terminal settings
STARTBGCOLOR="\[\033[48;2;255;190;0m\]"
STARTFGCOLOR="\[\033[38;2;20;20;28m\]"
ENDCOLOR="\[\033[0;0m\]"


# in case of error, change the bg color to red fg to white
ERRBGCOLOR="\[\033[48;2;85;0;0m\]"
ERRFGCOLOR="\[\033[38;2;255;255;255m\]"
ERRTXCOLOR="\[\033[38;2;85;0;0m\]"
ERRTXBGCOLOR="\[\033[48;2;20;20;28m\]"

POK="$LEFT$STARTBGCOLOR$LBGDK"$'\uE0B0'"$STARTFGCOLOR\w $ENDCOLOR"$'\uE0B0'" "
PNOK="$LEFT$ERRBGCOLOR$LBGDK"$'\uE0B0'"$ERRFGCOLOR\w $ERRTXCOLOR$ERRTXBGCOLOR"$'\uE0B0'"$ENDCOLOR "

# normal display

#PS1="$PNOK"

function prompt_command() {
    if [ $? = 0 ]; then
        PS1="$POK"
    else
        PS1="$PNOK"
    fi
}

export PROMPT_COMMAND=prompt_command
